<?php

class factorial_of_a_number
{
    protected $_number;

public function __construct($number)
{
    if (!is_int($number))
    {
        throw new InvalidArgumentException('Not a number or missing argument');

}
    $this->_number = $number;
}

    function result(){
        $result =1;
        for ($i=1;$i <=$this->_number;$i++){
            $result *=$i;
        }
        return $result;
    }

}

$newfactorial = New factorial_of_a_number(5);
echo $newfactorial->result();
